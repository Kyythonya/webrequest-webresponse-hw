﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;

namespace WebContentWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void GetContentButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var request = WebRequest.Create(urlResourceTextBox.Text);
                var response = await request.GetResponseAsync();

                string content;

                using (var stream = new StreamReader(response.GetResponseStream()))
                {
                    content = stream.ReadLine();
                    webContentTextBox.Text = $"{content}\n";
                }

                var vocabulary = new Dictionary<string, int>();
                foreach (var element in content.Split(
                    new char[] { ' ', ',', '.', '!', '?', '/', '\\', '}', '{', '-', '=', '+', '(', ')', '[', ']', '|', '<', '>', '@', '#', '$', '%', '^', ':', ';', '&', '*', '_', '№', '"', '\'', '`', '~' },
                    StringSplitOptions.RemoveEmptyEntries))
                {
                    if (vocabulary.ContainsKey(element))
                    {
                        vocabulary[element]++;
                    }
                    else
                    {
                        vocabulary.Add(element, 1);
                    }
                }

                foreach (KeyValuePair<string, int> pair in vocabulary)
                {
                    contentInfoTextBox.Text += $" element = {pair.Key} ; count =  {pair.Value}\n ";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
